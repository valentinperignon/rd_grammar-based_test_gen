\documentclass[a4paper, 11pt]{report}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{url}
\usepackage{tikz}
\usepackage{hyperref}
\usepackage[french]{babel}

\title{Recherche documentaire\\Test à partir de grammaires}
\author{Nathanaël Houn \and Valentin Perignon}
\date{Juin 2020}

\begin{document}
% ----- Titre -----
\begin{titlepage}

  \centering
  % Logos : UFR ST + Réseau Figure
  \begin{minipage}[t]{0.42\linewidth}
    \includegraphics[width=1\linewidth]{assets/logo_UFR-ST.png}\\[1.0 cm]
  \end{minipage}
  \hfill
  \begin{minipage}[t]{0.44\linewidth}
    \includegraphics[width=1\linewidth]{assets/logo_Reseau-CMI.png}\\[1.0 cm]
  \end{minipage}

  % Titre
  \vspace*{1.7 cm}
  \large Licence 2 Informatique -- Cursus Master en Ingénierie \\[0.05 cm]
  \large UFR Sciences et Techniques \\[3 cm]
  \Large \textbf{Recherche documentaire}\\[0.1 cm]
  \rule{\linewidth}{0.2 mm} \\[0.4 cm]
  \Huge\textbf{Test à partir de grammaires}
  \rule{\linewidth}{0.2 mm} \\[2.5 cm]

  % Date et auteur
  \large\textbf{Juin 2020}\\[1 cm]
  \Large Nathanaël \textsc{Houn}\\[0.1 cm]
  \Large Valentin \textsc{Perignon}\\[1.4 cm]

  % Tuteurs
  \Large \textsl{Tuteur}: M. Frédéric \textsc{Dadeau}

\end{titlepage}

% ----- Table des matières -----
\tableofcontents
\newpage

% ----- 1 : Qu'est-ce qu'une grammaire ? -----
\chapter{Qu'est-ce qu'une grammaire ?}

% -- Intro sur les grammaires --
Une grammaire est un ensemble de règles permettant d'accorder plusieurs lexèmes, aussi appelés symboles, ensemble afin
de former une chaîne. Elle nous indique toutes les associations possibles entre symboles pour former une chaîne
correcte.

La grammaire est beaucoup utilisée en informatique par les compilateurs. Elle leur permet de vérifier si la syntaxe du
code est correcte.

Plusieurs types de grammaire existent. Cependant, nous nous intéresserons uniquement aux
\textsl{grammaires hors-contexte} pour ce rapport.

% -- 1.1 Grammaire hors-contexte --
\section{Grammaire hors-contexte}

Une grammaire hors-contexte \cite{cfg-lecture}, ou \textsl{context-free grammar (CFG)}, est un type de grammaire
permettant de décrire un langage. Il s'agit d'un ensemble de règles permettant de générer une chaîne de caractères.
Elle est qualifiée de \textsl{hors-contexte} car chaque règle peut être appliquée indépendamment du contexte où elle
apparaît.

\noindent Une \textsl{CFG} peut être définie avec le 4-uplet \cite{lhc-lecture} suivant : (N, T, R, S).
\begin{description}
  \item[N]
        Un ensemble de symboles non-terminaux

  \item[T]
        Un ensemble de symboles terminaux, ce sont les lexèmes ($N \cap T = \emptyset$)

  \item[R]
        Un ensemble de règles de production sous la forme $R : N \rightarrow (N \cup T)$

  \item[S]
        Le symbole de départ, appelé \textsl{la racine}
\end{description}

% -- 1.2 Exemple de grammaire hors-contexte --
\section{Exemple de grammaire hors-contexte} \label{subsec:exemple-grammaire}
Pour comprendre ce qu'est une grammaire hors-contexte et de quels éléments elle se compose, nous pouvons utiliser un
exemple avec une grammaire (définie dans la figure \ref{fig:gram_maths}) permettant de définir une expression
mathématique avec des entiers. \\

\begin{figure}[ht]
  \begin{center}
    \begin{verbatim}
           O  := P | O + O | O - O
           P  := [L] | P * [L] | P / [L]
          [L] := 1..infini
\end{verbatim}
    \caption{Exemple de grammaire}
  \end{center}
  \label{fig:gram_maths}
\end{figure}

\noindent Le 4-uplet permettant de la définir est le suivant :
\begin{description}
  \item[N]
        \{ \texttt{O}, \texttt{P} \}

  \item[T]
        \{ \texttt{L} \}

  \item[R]
        \{ \texttt{O := P | O + O | O - O}, \texttt{P := [L] | P * [L] | P / [L]}, \texttt{[L] := 1..infini} \}

  \item[S]
        \texttt{O}
\end{description}

Cette grammaire comporte trois \textsl{règles de production}. La \textsl{racine}, ici \texttt{O}, correspond à la
première règle à utiliser pour former une opération mathématique. Pour chacune de ces règles, nous retrouvons deux
parties. Tout d'abord la partie gauche, c'est-à-dire \texttt{O}, \texttt{P} et \texttt{[L]}, qui correspond au
\textsl{symbole} de la règle, aussi appelé la \textsl{variable}. Lorsque celle-ci est entourée par des crochets, cela
signifie que c'est un \textsl{symbole terminal}. Ensuite, la partie droite qui est composée de lexèmes ou de règles de
dérivations. Lorsqu'il n'y a que des lexèmes, c'est le cas de la troisième règle \texttt{[S]}, le symbole est terminal,
cela signifie que nous ne pourrons pas dériver ce symbole après l'avoir transformé en lexème.\\

Nous pouvons utiliser cette grammaire pour vérifier si une expression mathématique est correctement formée. Pour cela,
nous analyserons l'expression suivante : \texttt{55 - 15 + 2} en utilisant une approche \textsl{bottom-up}. Cette
approche consiste à décomposer le texte en une suite de \textsl{lexèmes} puis à utiliser les
\textsl{règles de production} pour comprendre comment cette expression a été formée.

\texttt{55 - 15 + 2} est composée de 5 lexèmes : \texttt{\{55, -, 15, +, 2\}}. Il est possible de reconnaître facilement
\texttt{\{55, 15, 2\}} qui correspondent aux lexèmes du symbole terminal \texttt{[L]}. L'expression peut être 
transformée pour obtenir \texttt{[L] - [L] + [L]}.
Grâce à la \texttt{règle de production} \texttt{P}, nous pouvons remplacer \texttt{[L]} par \texttt{P}. Il est possible
de faire la même chose avec le symbole \texttt{O} pour obtenir \texttt{O - O + O}. Enfin, en utilisant
\textsl{la racine}, nous remarquons que nous pouvons utiliser les règles \texttt{O - O} et \texttt{O + O}. Ainsi, nous
sommes remontés jusqu'à \texttt{la racine} à partir de l'expression mathématique. Nous pouvons en déduire que cette 
dernière est correctement formée. Les dérivations qui ont permis de former l'expression peuvent être représentées par un
arbre (fig. \ref{fig:arbre_gram_expl}).

\begin{figure}
  \centering
  \begin{tikzpicture}
    \node {O}
    child { node {O - O}
        child { node {P}
            child { node {[L]}
                child { node {55} }
              }
          }
        child { node {O + O}
            child { node {P}
                child { node {[L]}
                    child { node {15} }
                  }
              }
            child { node {P}
                child { node {[L]}
                    child { node {2} }
                  }
              }
          }
      };
  \end{tikzpicture}
  \caption{Arbre représentant les dérivations}
  \label{fig:arbre_gram_expl}
\end{figure}

% ----- 2 : Que signifie tester ? -----
\chapter{Que signifie tester ?}

% -- 2.1 Utilité des tests --
\section{Utilité des tests}
En développement logiciel, les tests sont massivement utilisés afin d'aider à assurer la bonne qualité du logiciel développé.
D'une part, ils permettent d'identifier les \textit{bugs} qui apparaissent dans le programme pendant le développement,
et donc de permettre de les résoudre.
D'autre part, quand un \textit{bug} est identifié et résolu, on peut écrire un test qui échouera si le bug se manifeste à nouveau : cela permet de s'assurer que ce mauvais comportement du logiciel ne réapparaîtra plus par la suite. C'est ce qu'on appelle un test de non-régression \cite{wiki:test_regression}.

Les tests, une fois créés, s'exécutent et affichent un résumé facile à lire des éventuelles erreurs.
Ils sont souvent intégrés au \textsl{workflow} de développement et exécutés automatiquement avant la validation et
l'intégration du nouveau code dans le programme : ils font partie de l'intégration continue (en anglais, \textit{Continuous Integration}, ou \textit{CI}, dont on peut voir un exemple en figure \ref{fig:integration_continue}).
Ils sont donc un élément de plus en plus indispensable pour assurer la qualité du code et du produit développé. \\

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.7\textwidth]{assets/integration_continue.png}
  \caption{Résumé de tests dans un système d'Intégration Continue}
  \label{fig:integration_continue}
\end{figure}

% -- 2.2 Contenu d'un test --
\section{Contenu d'un test}
Le test logiciel a donc pour but de déceler un mauvais comportement du logiciel.
Pour cela, on va exécuter le système dans certaines configurations particulières, en vue de trouver des fautes.
Ces fautes du logiciel peuvent être variées : résultat incorrect, réaction inattendue pour l'utilisateur, ou même arrêt complet du programme.

Les configurations particulières peuvent être créées de plusieurs manières :
\begin{itemize}
  \item Si on a détecté un bug, alors on peut identifier comment le reproduire, c'est-à-dire trouver la situation qui provoque le bug, et intégrer cette situation dans un test, dans le cadre des tests de non-régression,
  \item Si, lors de l'écriture ou de la revue du code, on imagine une situation particulière, on peut écrire manuellement le test correspondant. C'est souvent le cas dans un \emph{test unitaire}, c'est-à-dire un test portant sur une partie très précise d'un programme, par exemple une fonction. On peut en voir un exemple dans la figure \ref{fig:execution_tests_unitaire},
  \item On peut générer des situations à l'aide d'outils de génération de tests : c'est cette procédure qui va nous intéresser dans les parties suivantes. \\
\end{itemize}

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.7\textwidth]{assets/tests_unitaires.png}
  \caption{Exécution de tests unitaires avec la bibliothèque googletest}
  \label{fig:execution_tests_unitaire}
\end{figure}

Il est souvent impossible de créer toutes les situations possibles pour tester un logiciel, car elles sont trop nombreuses.
Le test ne permet donc pas de s'assurer de l'absence complète d'erreur, autrement dit, du fonctionnement parfait du logiciel.
Il va donc falloir trouver des techniques pour générer les tests les plus intéressants possibles, afin de déceler un maximum de problèmes.

% -- 2.3 Tester à partir de grammaire --
\section{Tester à partir de grammaire}

Une solution pour générer des données de test automatiquement peut être de s'appuyer sur la grammaire.
Avant de voir plusieurs techniques qui permettent de créer des données à partir de \textsl{CFG}, identifions les critères qui définissent un bon ensemble de données de tests.
\begin{itemize}
  \item Le \emph{critère de couverture} : comme nous voulons tester un maximum de cas possible, les données doivent contenir le plus de combinaisons possible des variables de la \textsl{CFG},
  \item La quantité : nous voulons tester un maximum de cas, mais puisque chaque test prend du temps à s'exécuter, nous avons intérêt à en produire un nombre réduit. Ainsi, il sera possible de les exécuter en un temps raisonnable, par exemple dans un processus d'intégration continue,
  \item La personnalisation : beaucoup de choses dépendent de ce que l'on cherche à tester. Il est donc important que l'utilisateur puisse contrôler des paramètres comme la longueur de chaque donnée, le critère de couverture désiré ou l'utilisation ou non de certaines règles de la \textsl{CFG}.
\end{itemize}

% ----- 3 Approches utilisées pour le test de grammaire -----
\chapter{Approches utilisées pour le test de grammaires}

Nous allons maintenant nous intéresser à différentes méthodes utilisées pour générer des tests à partir de grammaires, en partant d'une approche simple pour nous approcher de techniques plus recherchées et produisant des tests de meilleure qualité.

% -- 3.1 Approche stochastique simple
\section{Approche stochastique simple}
Pour commencer, intéressons-nous à une première approche basée uniquement sur le hasard.
Reprenons la grammaire présentée dans la partie \ref{subsec:exemple-grammaire}, et générons aléatoirement une donnée conforme à cette grammaire dans le tableau \ref{fig:tab_generation_expression_math}.

\begin{figure}[ht]
  \centering
  \begin{tabular}{ c | c }
    \hline
    Étape & Donnée générée     \\ \hline \hline
    1     & \texttt{O}         \\ \hline
    2     & \texttt{P * [L]}   \\ \hline
    3     & \texttt{[L] * [L]} \\ \hline
    4     & \texttt{1 * 3}     \\ \hline
  \end{tabular}
  \caption{Génération simple d'une expression mathématique}
  \label{fig:tab_generation_expression_math}
\end{figure}

\begin{enumerate}
  \item Commençons par choisir un symbole aléatoirement parmi tous les symboles disponibles.
        Les probabilités pour chacun des symboles sont identiques.
        C'est \texttt{O} qui sort,
  \item On remplace le symbole tiré aléatoirement par une de ses définitions, que l'on choisit aussi avec un tirage au sort équiprobable,
  \item De nouveau, on remplace chacun des symboles non terminaux par une de ses définitions, jusqu'à qu'il n'y en aie plus. Ici, nous n'avons qu'un symbole non terminal : \texttt{P} est donc remplacé par une de ses définitions, tirée au hasard : \texttt{[L]},
  \item Maintenant que nous avons une expression qui ne contient que des symboles terminaux, nous pouvons remplacer chacun d'entre eux par une valeur tirée dans la définition de \texttt{[L]}, c'est-à-dire un entier entre 1 et $+\infty$.
\end{enumerate}

En quatre itérations, nous avons généré l'expression mathématique $1 * 3$, qui est bien syntaxiquement correcte dans la grammaire utilisée.
Nous avons pu aussi identifier une première limite de cette approche totalement stochastique : lors de l'étape 4, comment tirer un nombre au hasard dans une quantité infinie de nombres ?
Nous pouvons, par exemple, choisir de nous restreindre aux entiers codables sur 32 bits.
Ce choix reste dépendant du résultat voulu, et est plutôt arbitraire.
% TODO Nath « Bien vu, si vous avez une ou deux refs pour savoir comment on instancie un lexème ça peut être intéressant pour compléter.  »
Nous ne l'évoquerons donc pas plus ici. \\

Cette méthode permet de générer des expressions mathématiques syntaxiquement correctes (il sera impossible de se retrouver avec des expressions fausses comme $1 * / 3$ ou $1 + * \infty$).
Cependant, cela ne veut pas dire que toutes les données que nous allons générer seront sémantiquement correctes.
Par exemple, en suivant les étapes de la figure \ref{fig:tab_generation_expression_incorrecte}, nous obtenons la donnée $4 / 0$, correcte syntaxiquement, mais qui est une expression mathématique incorrecte.

\begin{figure}[ht]
  \centering
  \begin{tabular}{ c | c }
    \hline
    Étape & Donnée générée     \\ \hline \hline
    1     & \texttt{O}         \\ \hline
    2     & \texttt{P / [L]}   \\ \hline
    3     & \texttt{[L] / [L]} \\ \hline
    4     & \texttt{4 / 0}     \\ \hline
  \end{tabular}
  \caption{Génération simple d'une expression mathématique incorrecte}
  \label{fig:tab_generation_expression_incorrecte}
\end{figure}

Ce problème est inhérent aux grammaires hors contexte : il faudrait une règle supplémentaire du type « Ne pas remplacer le symbole terminal \texttt{[L]} par le nombre $0$ s'il est précédé du symbole $/$ ».
Mais cette sorte de règle de grammaire nécessite d'avoir une relation entre les différentes parties de l'expression générée \cite{5388302} : elle appartient donc aux \emph{grammaires en contexte}, qui sont hors du cadre de ce rapport. \\

En revanche, nous pouvons nous attarder sur d'autres problèmes de cette première approche.
Premièrement, nous n'avons aucune maîtrise sur la longueur de la donnée produite.
Lors de la première itération, nous aurions pu obtenir le symbole \texttt{[L]}, et alors la donnée produite aurait été plutôt courte.
Inversement, nous aurions pu aussi obtenir en boucle la valeur \texttt{O + O}, ce qui nous aurait plongé dans une boucle récursive infinie, et donc aucune donnée produite.
La façon la plus simple de résoudre ce problème est de mettre des limites sur la taille maximale de la donnée produite : au bout de X itérations, on arrête le programme et on remplace tous les symboles non terminaux par des symboles terminaux.
On peut aussi jouer sur les probabilités accordées à chaque règle \cite{Maurer90generatingtest} : en accordant une probabilité plus grande d'obtenir un symbole terminal (comme \texttt{[L]}) plutôt qu'une règle récursive (comme \texttt{O} et \texttt{P}) afin de limiter les chances d'obtenir une donnée extrêmement longue.
Cependant, ces méthodes restent des solutions limitées.

Le deuxième problème concerne le critère de couverture.
En effet, puisque le choix est fait aléatoirement à chaque étape, nous ne pouvons garantir qu'en produisant plusieurs données, celles-ci soient différentes.
Cela est un problème important, car dans l'objectif de tester, nous avons besoin de la plus grande diversité de données possibles afin de tester un maximum de cas différents.

Intéressons-nous donc maintenant à une approche plus affinée qui permet de supprimer ces problèmes.

% -- 3.2 Approche stochastique affinée --
\section{Approche stochastique affinée}

Cette deuxième approche, utilisée par Hai-Feng Guo et Zongyan Qiu \cite{agbtg}, est aussi une approche \textsl{stochastique}.
Cette méthode a pour avantage d’éviter les boucles lors des dérivations, phénomène qui complexifie le traitement, et de générer des chaînes de caractères bien distribuées au niveau des variables de la \textsl{CFG} utilisée.\\

Ces deux chercheurs présentent une méthode où chaque variable d'une \textsl{CFG} donnée est associée à un tuple
correspondant au degré de dérivation de la variable et un second tuple contenant la probabilité d'obtenir une variable
terminale à la prochaine dérivation. Pour comprendre le fonctionnement de cette stratégie, nous allons réutiliser la grammaire ci-dessus (fig. \ref{fig:arbre_gram_expl}).

Il est alors possible de représenter les tuples de cette grammaire hors-contexte à l'aide d'un tableau
(fig. \ref{fig:gram_tab_depart}). Le tuple de probabilités au degré de dérivation de O, un 3-uplet car cette variable
possède trois dérivations possibles, est égale à $(1, 1, 1)$ car nous n'avons encore pas dérivé. Le tuple de
probabilité, lui aussi un 3-uplet, est égal à $(0.33, 0.33, 0.33)$ car chacune des trois dérivations possibles de O a
la même chance d'être effectuée. \\

\begin{figure}[ht]
  \centering
  \begin{tabular}{ c || c | c }
    \hline
    Symbole & Degrés    & Probabilités       \\ \hline \hline
    O       & (1, 1, 1) & (0.33, 0.33, 0.33) \\ \hline
    P       & (1, 1, 1) & (0.33, 0.33, 0.33) \\ \hline
  \end{tabular}
  \caption{Tableau des tuples de départ}
  \label{fig:gram_tab_depart}
\end{figure}

Ensuite, une stratégie appelée \textsl{stratégie de doublement} est utilisée. Garder en mémoire le nombre de dérivations
d'une règle permet de calculer le tuple de probabilités. En effet, celui-ci suit la formule suivante :
$$p_i = \frac{w_i}{T} \text{ où } w_i = \frac{d_1}{d_i} \text{ et } T = \sum{i=1}^n w_i$$
En utilisant cette formule, si je dérive \texttt{O} pour obtenir \texttt{O + O}, mon tuple de degrés de dérivation devient $(1, 2, 1)$.
Nous pouvons calculer le tuple de probabilités pour obtenir $(0.4, 0.2, 0.4)$. Le degré de la deuxième règle a été
doublé. Pour les prochaines dérivations, la deuxième possibilité aura donc moins de chances
d'être sélectionnée. Nous pouvons remarquer ce phénomène en réalisant plusieurs dérivations et en observant les tuples de
la variable \texttt{O} (fig. \ref{fig:gram_tab_deriv}).

\begin{figure}[ht]
  \centering
  \begin{tabular}{ l || c | c }
    \hline
    Dérivation             & Degrés      & Probabilités         \\ \hline \hline
    \texttt{O}             & $(1, 1, 1)$ & $(0.33, 0.33, 0.33)$ \\ \hline
    \texttt{O + O}         & $(1, 2, 1)$ & $(0.4, 0.2, 0.4)$    \\ \hline
    \texttt{O - O + O}     & $(1, 2, 2)$ & $(0.5, 0.25, 0.25)$  \\ \hline
    \texttt{O + O - O + O} & $(1, 4, 2)$ & $(0.57, 0.14, 0.29)$ \\ \hline
    ...                    & ...         & ...                  \\ \hline
  \end{tabular}
  \caption{Tuples de \texttt{O} lors des dérivations}
  \label{fig:gram_tab_deriv}
\end{figure}

Cette méthode a donc pour avantage de chercher à éviter les boucles lors des dérivations, phénomène qui complexifie le traitement.\\

Lors de la génération de tests, un arbre de couverture est généré. Chaque nœud correspond à une étape de la dérivation
et les feuilles à une chaîne de caractères complète, résultat de la génération. Un nœud est composé de la chaîne de
caractères en cours de génération et du tuple de probabilités.

Lorsqu'une dérivation est terminée, la probabilité de cette règle est égale à 0 et les autres probabilités sont
recalculées. C'est le cas de la figure \ref{fig:coverage_tree}, le premier membre du tuple est à zéro car la règle
\texttt{P} a été utilisée puis \texttt{[L]}.

\begin{figure}
  \centering
  \begin{tikzpicture}
    \node {
      \begin{tabular}{ | c | c | c | }
        \hline
        \multicolumn{3}{|c|}{O} \\ \hline
        0,00 & 0,50 & 0,50      \\ \hline
      \end{tabular}
    }
    child {
        child{ node{[L]} }
        child{ node{...} }
        child{ node{...} }
      };
  \end{tikzpicture}
  \caption{Arbre de couverture}
  \label{fig:coverage_tree}
\end{figure}

Cela permet de générer des chaînes de caractères bien distribuées au niveau des variables de la \textsl{CFG} utilisée.

% ----- Conclusion -----
\chapter*{Conclusion}

Le test est un pan indispensable au développement de logiciels, son objectif est d'en améliorer la fiabilité.
L'écriture manuelle des tests est une tâche chronophage : c'est pourquoi on cherche à en automatiser une partie à l'aide
d'outils de génération de tests.

Ces outils se basent sur des grammaires hors-contexte, un ensemble de règles qui régissent, entre autres, la syntaxe de
langages utilisés par le logiciel.\\

La génération de tests à partir de grammaire étant un thème important dans le monde de la recherche en informatique,
elle s'est affinée avec le temps. Partis d'une méthode complètement stochastique, les outils actuels prennent en compte
de nombreux paramètres afin de produire des données de meilleure qualité, répondant à des critères de couverture et des
critères de quantité.

Pour arriver à ce résultat, des notions comme l'évolution de probabilité d'utilisation d'une règle au fur et à
mesure des dérivations et la stratégie de dédoublement ont été introduites dans les algorithmes de génération.\newline

Grâce aux approches que nous avons présentées, il est possible de générer de grandes quantités de tests utiles
automatiquement. Cependant, ces approches ne permettent pas de prévoir les résultats que devrait fournir le logiciel
pour chaque donnée. Cela est possible grâce à d'autres outils, notamment ceux utilisant la grammaire en contexte.

% ----- Table des figures -----
\newpage
\listoffigures

% ----- Bibliographie -----
\newpage
\bibliographystyle{plain}
\bibliography{rapport}

\end{document}
