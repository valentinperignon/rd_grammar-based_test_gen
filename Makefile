CC=pdflatex
TARGET=rapport.pdf

all: $(TARGET)

# Make report
rapport.pdf: rapport.tex rapport.aux rapport.bib
	bibtex rapport
	$(CC) $<
	$(CC) $<

%.aux: %.tex
	$(CC) $^

.PHONY: open, clean, mrproper
# Only for power users (i.e. Mac)
open: all
	open $(TARGET)

# House cleaning
clean:
	rm -f *.aux *.log *.lof *.toc
	rm -f *.bbl *.blg

mrproper: clean
	rm -f *.pdf
